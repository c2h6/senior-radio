# senior-radio

Build a simple FM (maybe also DAB+) radio which is easy to use, multilingual, big simple display, and has very good reception.

## possible components
https://www.exp-tech.de/displays/e-paper-e-ink/8886/waveshare-4.2-e-ink-raw-display-three-color-400x300?c=1424
e-ink or OLED display?

http://hjberndt.de/soft/rdsradioclock.html

Meanwhile I installed and started using this, looks and works great so far:
https://github.com/Edzelf/ESP32-Radio
